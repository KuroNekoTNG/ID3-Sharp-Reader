﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ID3_Sharp_Reader;
using System.Collections.Generic;

namespace Reader_Unit_Test {
	[TestClass]
	public class UnitTest1 {

		public static readonly string LOG_FOLDER = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.Desktop ), "ID3 Test Folder" );

		[TestMethod]
		public void StaticMembersCount() {
			if ( !Directory.Exists( LOG_FOLDER ) ) {
				Directory.CreateDirectory( LOG_FOLDER );
			}

			FileStream fileStream = File.Create( Path.Combine( LOG_FOLDER, "Test1.log" ) );
			StringBuilder stringBuilder = new StringBuilder( $"Tag size is {Tags.STRING_TAGS.Length}\n" );

			int count = 0, totalCount = 0;
			char lastChar = ' ';

			foreach ( string tag in Tags.STRING_TAGS ) {
				if ( tag.StartsWith( "A" ) ) {
					if ( lastChar != 'A' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'A';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "C" ) ) {
					if ( lastChar != 'C' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'C';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "E" ) ) {
					if ( lastChar != 'E' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'E';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "G" ) ) {
					if ( lastChar != 'G' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'G';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "L" ) ) {
					if ( lastChar != 'L' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'L';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "M" ) ) {
					if ( lastChar != 'M' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'M';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "O" ) ) {
					if ( lastChar != 'O' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'O';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "P" ) ) {
					if ( lastChar != 'P' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'P';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "R" ) ) {
					if ( lastChar != 'R' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'R';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "S" ) ) {
					if ( lastChar != 'S' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'S';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "T" ) ) {
					if ( lastChar != 'T' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'T';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "U" ) ) {
					if ( lastChar != 'U' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'U';
						count = 0;
					}

					count++;
					totalCount++;
				} else if ( tag.StartsWith( "W" ) ) {
					if ( lastChar != 'W' ) {
						stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\n" );

						lastChar = 'W';
						count = 0;
					}

					count++;
					totalCount++;
				}
			}

			stringBuilder.Append( $"Amount of '{lastChar}' of tags is {count}.\nTotal count is {totalCount}." );

			WriteToLog( stringBuilder.ToString(), fileStream );
		}
		[TestMethod]
		public void TagCompTest1() {
			FileStream fileStream = File.Create( Path.Combine( LOG_FOLDER, "Test2.log" ) );
			DateTime now = DateTime.Now;
			Random random = new Random();

			for ( int i = 0; i < 5; i++ ) {
				Tags.IsKnownTag( Tags.STRING_TAGS[ random.Next( Tags.STRING_TAGS.Length ) ] );
			}

			WriteToLog( $"Took 5 rounds of calling IsKnownTag ~{( DateTime.Now - now )}.", fileStream );

			now = DateTime.Now;

			Tags.IsKnownTag( "XXXX" );

			WriteToLog( $"Took ~{DateTime.Now - now} to validate if 'XXXX' was a valid tag.", fileStream );
		}
		[TestMethod]
		public void TagCompTest2() {
			FileStream fileStream = File.Create( Path.Combine( LOG_FOLDER, "Test3.log" ) );
			DateTime now = DateTime.Now;
			Random random = new Random();

			for ( int i = 0; i < 5; i++ ) {
				string[] tags = { Tags.STRING_TAGS[ random.Next( Tags.STRING_TAGS.Length ) ], Tags.STRING_TAGS[ random.Next( Tags.STRING_TAGS.Length ) ],
					Tags.STRING_TAGS[ random.Next( Tags.STRING_TAGS.Length ) ], Tags.STRING_TAGS[ random.Next( Tags.STRING_TAGS.Length ) ],
					Tags.STRING_TAGS[ random.Next( Tags.STRING_TAGS.Length ) ] };

				Tags.IsAnyKnownTag( tags, out string[] dummy );
			}

			WriteToLog( $"Took 5 rounds of calling IsAnyKnownTag ~{( DateTime.Now - now )}.", fileStream );
		}
		[TestMethod]
		public void TestTagLoader() {
			FileStream fileStream = File.Create( Path.Combine( LOG_FOLDER, "Test4.log" ) ),
				testImage = null;

			ID3_TAGS[] tags = { ID3_TAGS.APIC, ID3_TAGS.TIT2, ID3_TAGS.TALB };

			string[] songs = GatherAllSongs( Environment.GetFolderPath( Environment.SpecialFolder.MyMusic ) );
			int songNum = 0;

			foreach ( string songPath in songs ) {
				byte[] file = File.ReadAllBytes( songPath );

				testImage = File.Create( Path.Combine( LOG_FOLDER, $"TestImage{songNum}.jpg" ) );

				TagLoader tagLoader = new TagLoader( file, tags );

				DateTime now = DateTime.Now;

				try {
					tagLoader.LoadTag();
				} catch ( Exception e ) {
					WriteToLog( $"Stacktrace: {e.StackTrace}\n\tMessage: {e.Message}", fileStream );
				}

				WriteToLog( $"Took {DateTime.Now - now} to complete the loading of tags of {songPath.Substring( songPath.LastIndexOf( Path.DirectorySeparatorChar ) + 1 )}.\n{songNum}: {tagLoader.ToString()}", fileStream );

				byte[] bytes = tagLoader.GetTag( ID3_TAGS.APIC ) as byte[];

				try {
					testImage.Write( bytes, 0, bytes.Length );
				} catch ( Exception ) { }
				testImage.Close();
				songNum++;
			}

		}

		private string[] GatherAllSongs( string path ) {
			List<string> music = new List<string>();
			DirectoryInfo directoryInfo = new DirectoryInfo( path );

			foreach ( FileSystemInfo child in directoryInfo.EnumerateFileSystemInfos() ) {
				if ( child.Attributes.HasFlag( FileAttributes.Directory ) ) {
					music.AddRange( GatherAllSongs( child.FullName ) );
				} else {
					FileInfo child2 = new FileInfo( child.FullName );

					switch ( child2.Extension ) {
						case ".mp3": music.Add( child.FullName ); break;
						case ".wav": music.Add( child.FullName ); break;
					}
				}
			}

			return music.ToArray();
		}

		private void WriteToLog( string text, FileStream fileStream ) {
			text += "\n";

			byte[] textBytes = Encoding.Unicode.GetBytes( text );

			fileStream.Write( textBytes, 0, textBytes.Length );

			fileStream.Flush();
		}
	}
}
