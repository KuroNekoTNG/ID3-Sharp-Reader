﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ID3_Sharp_Reader {
	public class TagLoader {

		private string path;
		private byte[] data; // Stores the file's data.
		private Dictionary<ID3_TAGS, object> tagData; // Used to hold the wanted tags.

		#region Constructors

		public TagLoader( string path, ID3_TAGS[] tags, bool loadFromConstructor ) : this( path, tags ) {
			LoadTag();
		}

		public TagLoader( byte[] data, ID3_TAGS[] tags, bool loadFromConstructor ) : this( data, tags ) {
			LoadTag();
		}
		// This contructor is a lazy bastard! It just calls another contsructor then proceeds to call a method.
		// I can't believe the nerve of this constructor! Having another method do your dirty work? How dare you!
		public TagLoader( string path, ID3_TAGS[] tags ) : this( path ) {
			AddTags( tags, false );
		}
		// Here's another lazy bastard!
		public TagLoader( byte[] data, ID3_TAGS[] tags ) : this( data ) {
			AddTags( tags, false );
		}
		// This constructor takes in a path, then checks to see if it's a valid file and it exists.
		// If the file doesn't exist, it will throw a FileNotFoundException.
		public TagLoader( string path ) {
			this.path = path;
			// Checks to see if the file acturally exists. If it doesn't, throws a FileNotFoundException.
			if ( !File.Exists( path ) ) {
				throw new FileNotFoundException( "Expected a file, got nothing or a directory." );
			} else {
				// Assigns the output of File.ReadAllBytes to a byte array.
				data = File.ReadAllBytes( path );
				// Creates the dictonary that the data will be stored in.
				tagData = new Dictionary<ID3_TAGS, object>();
			}
		}
		// This constructor takes in a byte[] data and stores it for later.
		public TagLoader( byte[] data ) {
			// Assigns this.data to the passed data.
			this.data = data;
			// Creates an empty dictonary that data will be stored in.
			tagData = new Dictionary<ID3_TAGS, object>();
		}

		#endregion Constructors

		#region Tag manipulators
		// This method takes an array of tags and a boolean.
		// If the boolean is true, one it's done adding the new tags, it will then proceed to reload the file into memory,
		// dump all it's contents to garbage collection, and re do the loading process.
		public void AddTags( ID3_TAGS[] tags, bool reload ) {
			// Loops through the array to add every single tag to the dictionary.
			foreach ( ID3_TAGS tag in tags ) {
				if ( !tagData.ContainsKey( tag ) ) {
					AddTag( tag, false );
				}
			}
			// Reloads the data stored if path is not empty and not null.
			if ( reload && path != null && path != "" ) {
				data = File.ReadAllBytes( path );
			}
		}
		// Add's a tag to the dictionary. If reload is true, it will redo the entire loading process.
		public void AddTag( ID3_TAGS tag, bool reload ) {
			// Checks to see if the tag is already a key, and if so, ignore's adding it.
			if ( !tagData.ContainsKey( tag ) ) {
				tagData.Add( tag, null );
			}
			// Reloads the data stored if path is not empty and not null.
			if ( reload && path != null && path != "" ) {
				data = File.ReadAllBytes( path );
			}
		}
		// Returns the data stored by that tag. Returns null if the tag is not a key.
		public object GetTag( ID3_TAGS tag ) {
			if ( tagData.ContainsKey( tag ) ) {
				return tagData[ tag ];
			} else {
				return null;
			}
		}
		// Calls the main version of this method.
		public object GetTag( string tag ) {
			return GetTag( Tags.ConvertStringToTag( tag ) );
		}

		#endregion Tag manipulators
		// Starts the loading process of gathering tag data.
		public void LoadTag() {
			int index = 0, frameEnd = 0;
			// If the IndexOutOfRangeException is thrown, it's to be assumed that it hit the end of the file.
			try {
				// Loops through the entire data and checks for a tag.
				while ( index < data.Length ) {
					// Assigns index to the returned value from FindFrame.
					index = FindFrame( frameEnd );
					// Breaks if index is equal to -1.
					if ( index == -1 ) {
						break;
					}
					// Try-Catch to prevent the processing of the data from inturupting.
					try {
						// Output of FindFrames is inputed into this method.
						frameEnd = ProcessFrame( index, out string tag );
					} catch ( Exception ) {
						// If method does throw an exception, the catch changes frameEnd by 4 plus the index.
						frameEnd = index + 4;
					}
				}
			} catch ( IndexOutOfRangeException ) {
				// Do nothing.
			}
		}
		// ProcessFrame is where all the magic happens. It takes the start of the from. Figures out the tag, and from there, processes it accordinally.
		private int ProcessFrame( int offset, out string tag ) {
			// Tag is gotten from the first 4 bytes.
			tag = Tags.GetStringTagFromBytes( new byte[] { data[ offset ], data[ ++offset ], data[ ++offset ], data[ ++offset ] } );
			// Size is initilized to 0.
			int size = 0;
			// Checks to see if it's an APIC (For some reason, they follow the 8 byte rule where as other tags don't). If the tag isn't APIC, it uses the standard.
			if ( Tags.ConvertStringToTag( tag ) == ID3_TAGS.APIC ) {
				size = ( data[ ++offset ] << 24 ) | ( data[ ++offset ] << 16 ) | ( data[ ++offset ] << 8 ) | ( data[ ++offset ] << 0 );
			} else {
				size = ( data[ ++offset ] << 21 ) | ( data[ ++offset ] << 14 ) | ( data[ ++offset ] << 7 ) | ( data[ ++offset ] << 0 );
			}
			// Skips the flags, fuck the flags, we've loaded this data into RAM motherfucker!
			offset += 2;
			// Get's the encoding type.
			Encoding encoding = Tags.GetEncoding( data[ ++offset ] );
			// Checks to see if the BigEdian/SmallEdian marks are there, if not, reset's offset by 2.
			if ( ( data[ ++offset ] != 0xff || data[ offset ] != 0xfe ) && ( data[ ++offset ] != 0xff || data[ offset ] != 0xfe ) ) {
				offset -= 2;
			}
			// Set's a boolean to true or false depending on reather the tag is APIC or not.
			bool isNotAPIC = Tags.ConvertStringToTag( tag ) != ID3_TAGS.APIC;
			// Creates a list to dynamically store the data.
			List<byte> frame = new List<byte>();
			// Itterates over the data until i is the size plus the offset, or until the termination flags are found.
			for ( int i = ++offset; i < offset + size - 1; i++ ) {
				if ( isNotAPIC ) {
					if ( ( encoding == Tags.ISO_8859_1 || encoding == Tags.UTF_8 ) && data[ i ] == 0x00 ) {
						break;
					} else if ( data[ i ] == 0x00 && data[ i + 1 ] == 0x00 ) {
						break;
					}
				}

				frame.Add( data[ i ] );
			}
			// If the frame isn't an APIC frame, it will load it as a string, otherwise it will call a method dedicated to loading the APIC image.
			if ( isNotAPIC ) {
				tagData[ Tags.ConvertStringToTag( tag ) ] = encoding.GetString( frame.ToArray() );
			} else {
				tagData[ Tags.ConvertStringToTag( tag ) ] = ProcessAPIC( frame.ToArray() );
			}
			// Returns the offset plus the size minus one to ensure it doesn't skip a tag.
			return offset + size - 1;
		}
		// Processes the APIC data and retrives the image from it.
		private byte[] ProcessAPIC( byte[] data ) {
			// Initializes offset to zero.
			int offset = 0;
			// Flag to start addind data to the image.
			bool image = false;
			// List to add the data to dynamically.
			List<byte> byteList = new List<byte>();
			// Itterates over the data it was given.
			for ( offset = 0; offset < data.Length; offset++ ) {
				// Checks to see if the image flag wasn't tripped and to see if it matches any of the recommended image types for ID3 tags.
				if ( !image && Tags.IsID3RecImage( new byte[] { data[ offset ], data[ offset + 1 ], data[ offset + 2 ], data[ offset + 3 ] } ) ) {
					// If true, it will trip the boolean flag and add the data it's looking at to the list.
					image = true;
					byteList.Add( data[ offset ] );
				} else if ( image ) {
					// Otherwise it will either continue looping or if the flag was tripped add data to the list until it hits the end.
					byteList.Add( data[ offset ] );
				}
			}
			// Returns the list as a byte array.
			return byteList.ToArray();
		}
		// Looks for the frame and returns the starting location.
		private int FindFrame( int offset ) {
			// Looks through the data for any known tags.
			for ( int i = offset; i < data.Length - 4; i++ ) {
				// Creats an array of the four bytes from i including i.
				byte[] byteTag = { data[ i ], data[ i + 1 ], data[ i + 2 ], data[ i + 3 ] };
				// Checks to see if those bytes are a known tag. If so, it will check to see if it needs that tag.
				if ( Tags.IsKnownTag( byteTag ) ) {
					// Converts the byte array to a string and from a string to an ID3_TAGS.
					ID3_TAGS knownTag = Tags.ConvertStringToTag( Tags.ConvertToString( byteTag, 0x00 ) );
					// Itterates over the keys to see if it needs the tag.
					foreach ( ID3_TAGS tag in tagData.Keys ) {
						if ( knownTag == tag ) {
							return i;
						}
					}
				}
			}
			// If it hit's the end, return -1 to signify that no tag has been found.
			return -1;
		}
		// The overriden ToString method mainly used to debug this program.
		public override string ToString() {
			StringBuilder stringBuilder = new StringBuilder();

			stringBuilder.AppendLine( $"TagLoader:" );

			foreach ( ID3_TAGS tag in tagData.Keys ) {
				try {
					if ( tag == ID3_TAGS.APIC ) {
						stringBuilder.Append( $"\t{Enum.GetName( typeof( ID3_TAGS ), tag )}:\n\t\t" );

						byte[] data = tagData[ ID3_TAGS.APIC ] as byte[];

						for ( int i = 0; i < data.Length; i++ ) {
							stringBuilder.Append( $"{data[ i ].ToString( "X" )} " );

							if ( i % 30 == 0f && i != 0 ) {
								stringBuilder.Append( "\n\t\t" );
							}
						}

						stringBuilder.AppendLine();
					} else {
						stringBuilder.AppendLine( $"\t{Enum.GetName( typeof( ID3_TAGS ), tag )}: {tagData[ tag ]}" );
					}
				} catch ( NullReferenceException ) {
					stringBuilder.AppendLine( "NULL" );
				}
			}

			return stringBuilder.ToString();
		}
	}
}