﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ID3_Sharp_Reader {
	public enum ID3_TAGS {
		AENC, APIC, ASPI, COMM, COMR, ENCR, EQU2, ETCO, GEOB, GRID, LINK, MCDI, MLLT, OWNE, PRIV, PCNT, POPM, POSS, PBUF, RVA2, RVRB, SEEK, SIGN, SYLT, SYTC, TALB,
		TBPM, TCOM, TCON, TCOP, TDEN, TDLY, TDOR, TDRC, TDLR, TDTG, TENC, TEXT, TFLT, TIPL, TIT1, TIT2, TIT3, TKEY, TLAN, TLEN, TMCL, TMED, TMOO, TOAL, TONF, TOLY,
		TOPE, TOWN, TPE1, TPE2, TPE3, TPE4, TPOS, TPRO, TPUB, TRCK, TRSN, TRSO, TSOA, TSOP, TSOT, TSRC, TSSE, TSST, TXXX, UFID, USER, USLT, WCOM, WCOP, WOAF, WOAR,
		WOAS, WORS, WPAY, WPUB, WXXX, XXXX
	}

	public static class Tags {
		public static readonly string[] STRING_TAGS = { "AENC", "APIC", "ASPI", "COMM", "COMR", "ENCR", "EQU2", "ETCO", "GEOB", "GRID", "LINK", "MCDI", "MLLT",
			"OWNE", "PRIV", "PCNT", "POPM", "POSS", "PBUF", "RVA2", "RVRB", "SEEK", "SIGN", "SYLT", "SYTC", "TALB", "TBPM", "TCOM", "TCON", "TCOP", "TDEN", "TDLY",
			"TDOR", "TDRC", "TDLR", "TDTG", "TENC", "TEXT", "TFLT", "TIPL", "TIT1", "TIT2", "TIT3", "TKEY", "TLAN", "TLEN", "TMCL", "TMED", "TMOO", "TOAL", "TONF",
			"TOLY", "TOPE", "TOWN", "TPE1", "TPE2", "TPE3", "TPE4", "TPOS", "TPRO", "TPUB", "TRCK", "TRSN", "TRSO", "TSOA", "TSOP", "TSOT", "TSRC", "TSSE", "TSST",
			"TXXX", "UFID", "USER", "USLT", "WCOM", "WCOP", "WOAF", "WOAR", "WOAS", "WORS", "WPAY", "WPUB", "WXXX" };
		public static readonly Encoding ISO_8859_1 = Encoding.GetEncoding( "ISO-8859-1" ),
			UTF_16 = Encoding.GetEncoding( "UTF-16" ),
			UTF_16BE = Encoding.GetEncoding( "UTF-16BE" ),
			UTF_8 = Encoding.UTF8;

		public static ID3_TAGS ConvertStringToTag( string tag ) {
			int start = GetPoints( tag, out int end );

			foreach ( ID3_TAGS id3_tag in Enum.GetValues( typeof( ID3_TAGS ) ) ) {
				if ( Enum.GetName( typeof( ID3_TAGS ), id3_tag ).Equals( tag ) ) {
					return id3_tag;
				}
			}

			return ID3_TAGS.XXXX;
		}

		public static string GetStringTagFromBytes( byte[] data, byte encoding = 0x00 ) {
			if ( data.Length != 4 ) {
				throw new Exception( "Variable data must be 4 bytes big." );
			}

			string strTag = ConvertToString( data, encoding );

			if ( IsKnownTag( strTag ) ) {
				return strTag;
			} else {
				return null;
			}
		}

		#region Singular Checks
		// This method checks a tag in byte form to see if it's a valid tag, the default encoding is UTF-8.
		// Returns true if it is a valid known tag, otherwise false.
		public static bool IsKnownTag( byte[] tag, byte encoding = 0x03 ) {
			// Checks to see if tag is 4 bytes big. If not, throw an exception.
			if ( tag.Length != 4 ) {
				throw new Exception( "Variable tag must be 4 bytes big, otherwise it's not an ID3v2 tag, dumbass." );
			}
			string strTag = Encoding.Default.GetString( tag );
			// Returns the boolean from the method below.
			return IsKnownTag( strTag );
		}
		// / \
		//  |
		// Diddo except with a string and no encoding parameter.
		// It's best to use the IsKnownTag( byte[] tag, byte encoding ) since it does call this method and it will ensures exactness.
		// Assumes encoding is Unicode.
		public static bool IsKnownTag( string tag ) {
			int start = 0;

			start = GetPoints( tag, out int end );
			// Itterates over the STRING_TAGS array and checks only the strings it needs to.
			for ( int i = start; i < end; i++ ) {
				if ( STRING_TAGS[ i ].Equals( tag ) ) {
					return true;
				}
			}

			return false;
		}

		#endregion Singular Checks

		#region Multiple Checks
		// Checks a list of maybe valid tags against the known tags.
		// If a valid tag is found it is put into the areKnownTags and are given back with the boolean result.
		public static bool IsAnyKnownTag( List<string> tags, out List<string> areKnownTags ) {
			// Calls one of the overrides first, stores the return in a variable, and inlines the out.
			bool areValidTags = IsAnyKnownTag( tags, out string[] areTags );
			// Checks to see if the areValidTags boolean is true, if so, create a list from the areTags array.
			// If not, do not create a new instance of a list and assign it null.
			if ( areValidTags ) {
				areKnownTags = new List<string>( areTags );
			} else {
				areKnownTags = null;
			}

			return areValidTags;
		}
		// Calls one of the override versions of this method with the exact varible types with minimal extra calculations.
		public static bool IsAnyKnownTag( List<string> tags, out string[] areKnownTags ) {
			return IsAnyKnownTag( tags.ToArray(), out areKnownTags );
		}
		// Calls one of the override versions of this method with the exact varible types with minimal extra calulations.
		public static bool IsAnyKnownTag( string[] tags, out List<string> areKnownTags ) {
			return IsAnyKnownTag( new List<string>( tags ), out areKnownTags );
		}
		// The base method that all the other overrides call.
		public static bool IsAnyKnownTag( string[] tags, out string[] areKnownTags ) {
			// Creates a list of string to store all the right tags.
			List<string> areTags = new List<string>();
			// Loops through all the tags checks them with the IsKnownTag method.
			foreach ( string tag in tags ) {
				// If method returns true, add the tag to the List<string>, otherwise, do nothing with it.
				if ( IsKnownTag( tag ) ) {
					areTags.Add( tag );
				}
			}
			// Checks to see if the list is greater than 0, if true, makes the areKnownTags assigns it to the output of areTags.ToArray() and returns true.
			// Otherwise it set's areKnownTags to null and returns false.
			if ( areTags.Count > 0 ) {
				areKnownTags = areTags.ToArray();
				return true;
			} else {
				areKnownTags = null;
				return false;
			}
		}

		#endregion Multiple Checks
		// Converts anything to Unicode.
		public static string ConvertToUnicode( byte[] bytes, Encoding srcEncoding ) {
			if ( srcEncoding != Encoding.Unicode ) {
				bytes = Encoding.Convert( srcEncoding, Encoding.Unicode, bytes );
			}

			return srcEncoding.GetString( bytes );
		}

		public static string ConvertToString( byte[] data, byte byteEncoding ) {
			// Get's the encoding from the byte that was passed to it.
			Encoding encoding = GetEncoding( byteEncoding );
			// Returns the returned result of the encoding.GetString.
			return encoding.GetString( data );
		}

		public static Encoding GetEncoding( byte encoding ) {
			switch ( encoding ) {
				case 0x00:
					return ISO_8859_1;
				case 0x01:
					return UTF_16;
				case 0x02:
					return UTF_16BE;
				case 0x03:
					return UTF_8;
				default:
					throw new Exception( "Invalid encoding type of ID3v2." );
			}
		}

		public static bool IsBigEdian( byte byte1, byte byte2 ) {
			return ( byte1 == 0xFF ) && ( byte2 == 0xFE );
		}

		public static bool IsID3RecImage( byte[] bytes4 ) {
			if ( ( bytes4[ 0 ] == 0xff && bytes4[ 1 ] == 0xd8 && bytes4[ 2 ] == 0xff )
				|| ( bytes4[ 0 ] == 0x89 && bytes4[ 1 ] == 0x50 && bytes4[ 2 ] == 0x4e && bytes4[ 3 ] == 0x47 ) ) {
				return true;
			} else {
				return false;
			}
		}

		private static int GetPoints( string tag, out int end ) {
			// This if tree helps cut the processing time down by only looking at the potion of the list or, if it's the only type, the string itself.
			int start = 0;
			if ( tag.StartsWith( "A" ) ) {
				end = 3;
			} else if ( tag.StartsWith( "C" ) ) {
				start = 3;
				end = 5;
			} else if ( tag.StartsWith( "E" ) ) {
				start = 5;
				end = 8;
			} else if ( tag.StartsWith( "G" ) ) {
				start = 8;
				end = 10;
			} else if ( tag.Equals( STRING_TAGS[ 10 ] ) ) {
				start = 10;
				end = 11;
			} else if ( tag.StartsWith( "M" ) ) {
				start = 11;
				end = 13;
			} else if ( tag.Equals( STRING_TAGS[ 13 ] ) ) {
				start = 13;
				end = 14;
			} else if ( tag.StartsWith( "P" ) ) {
				start = 14;
				end = 19;
			} else if ( tag.StartsWith( "R" ) ) {
				start = 19;
				end = 21;
			} else if ( tag.StartsWith( "S" ) ) {
				start = 21;
				end = 25;
			} else if ( tag.StartsWith( "T" ) ) {
				start = 25;
				end = 71;
			} else if ( tag.StartsWith( "U" ) ) {
				start = 71;
				end = 74;
			} else if ( tag.StartsWith( "W" ) ) {
				start = 74;
				end = 83;
			} else {
				end = -1;
			}

			return start;
		}
	}
}